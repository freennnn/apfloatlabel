//
//  FloatLabelTextField.m
//  FloatLabel
//
//  Created by alexander.pranevich on 3/5/15.
//  Copyright (c) 2015 AP. All rights reserved.
//

#import "FloatLabelTextField.h"

const CGFloat kTextFieldTextInset = 10.0f;
const CGFloat kFloatLabelAnimationDuration = 0.2f;

@implementation FloatLabelTextField

#pragma mark - Life cycle

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setup];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
        [self setupUI];
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self setupUI];
    
    //manually call custom setters for properties from IB
    self.placeholder = self.placeholder;
    self.text = self.text;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Private methods

- (void)setup
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(textFieldTextDidChange:)
                                                 name:UITextFieldTextDidChangeNotification
                                               object:self];
}

- (void)setupUI
{
    self.floatLabel = [[UILabel alloc] initWithFrame:[self frameForFloatLabel]];
    self.floatLabel.font = [UIFont systemFontOfSize:10];
    if (!self.floatLabelHighligtedColor) {
        self.floatLabelHighligtedColor = [UIColor colorWithRed:48/255.0 green:171/255.0 blue:218/255.0 alpha:1];
    }
    if (!self.floatLabelColor) {
        self.floatLabelColor = [UIColor grayColor];
    }
    self.floatLabel.alpha = 0;
    [self addSubview:self.floatLabel];
}

- (CGRect)frameForFloatLabel
{
    return CGRectMake(0, 0, self.frame.size.width, floorf((self.frame.size.height - self.font.lineHeight)/2) + kTextFieldTextInset);
}

- (void)showFloatLabelAnimated:(BOOL)animated
{
    if (animated) {
        UIViewAnimationOptions options = UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationOptionCurveEaseIn;
        [UIView animateWithDuration:kFloatLabelAnimationDuration delay:0.f options:options animations:^{
            self.floatLabel.alpha = 1;
        } completion:nil];
    }
    else {
        self.floatLabel.alpha = 1;
    }
}

- (void)hideFloatLabelAnimated:(BOOL)animated
{
    if (animated) {
        UIViewAnimationOptions options = UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationOptionCurveEaseOut;
        [UIView animateWithDuration:kFloatLabelAnimationDuration delay:0.f options:options animations:^{
            self.floatLabel.alpha = 0;
        } completion:nil];
    }
    else {
        self.floatLabel.alpha = 0;
    }
}

- (void)changeFloatLabelColorAnimated:(BOOL)animated withBlock:(void (^)(void))animationBlock
{
    if (animated) {
        UIViewAnimationOptions options = UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationOptionTransitionCrossDissolve;
        [UIView transitionWithView:self.floatLabel duration:kFloatLabelAnimationDuration options:options animations:^{
            animationBlock();
        } completion:nil];
    }
    else {
        animationBlock();
    }
}

#pragma mark - Overriden methods

- (void)setText:(NSString *)text
{
    [super setText:text];
    if ([text length] > 0) {
        self.floatLabel.textColor = self.floatLabelColor;
        [self showFloatLabelAnimated:YES];
    }
}

- (void)setPlaceholder:(NSString *)placeholder
{
    [super setPlaceholder:placeholder];
    if ([placeholder length] > 0) {
        self.floatLabel.text = placeholder;
    }
}

- (CGRect)textRectForBounds:(CGRect)bounds
{
    return [super textRectForBounds:UIEdgeInsetsInsetRect(bounds, UIEdgeInsetsMake(kTextFieldTextInset, 0.f, 0.f, 0.f))];
}

- (CGRect)editingRectForBounds:(CGRect)bounds
{
    return [self textRectForBounds:bounds];
}

#pragma mark - UIResponder overriden methods

- (BOOL)becomeFirstResponder
{
    [super becomeFirstResponder];
    [self changeFloatLabelColorAnimated:YES withBlock:^{
        self.floatLabel.textColor = self.floatLabelHighligtedColor;
    }];
    return YES;
}

- (BOOL)resignFirstResponder
{
    [super resignFirstResponder];
    [self changeFloatLabelColorAnimated:YES withBlock:^{
        self.floatLabel.textColor = self.floatLabelColor;
    }];
    return YES;
}

#pragma mark - TextField observer

- (void)textFieldTextDidChange:(NSNotification *)notification
{
    if (notification.object == self) {
        BOOL isFloatLabelHidden = (self.floatLabel.alpha == 0);
        if ([self.text length] > 0) {
            if (isFloatLabelHidden) {
                [self showFloatLabelAnimated:YES];
            }
        }
        else {
            if (!isFloatLabelHidden) {
                [self hideFloatLabelAnimated:YES];
            }
        }
    }
}

@end
