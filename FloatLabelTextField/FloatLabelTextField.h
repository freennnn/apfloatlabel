//
//  FloatLabelTextField.h
//  FloatLabel
//
//  Created by alexander.pranevich on 3/5/15.
//  Copyright (c) 2015 AP. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FloatLabelTextField : UITextField
@property (nonatomic, strong) UILabel *floatLabel;
@property (nonatomic, strong) IBInspectable UIColor *floatLabelHighligtedColor;
@property (nonatomic, strong) IBInspectable UIColor *floatLabelColor;

@end
