//
//  ViewController.m
//  FloatLabel
//
//  Created by alexander.pranevich on 3/5/15.
//  Copyright (c) 2015 AP. All rights reserved.
//

#import "ViewController.h"
#import "FloatLabelTextField.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    CGRect frame = CGRectMake(20, 20, 150, 40);
    FloatLabelTextField *floatLabelTextField1 = [[FloatLabelTextField alloc] initWithFrame:frame];
    floatLabelTextField1.placeholder = @"First texfield";
    floatLabelTextField1.text = @"Predefined text";
    floatLabelTextField1.font = [UIFont fontWithName:@"HelveticaNeue" size:20];
    [self.view addSubview:floatLabelTextField1];
    
    frame = CGRectMake(20, 70, 150, 40);
    FloatLabelTextField *floatLabelTextField2 = [[FloatLabelTextField alloc] initWithFrame:frame];
    floatLabelTextField2.placeholder = @"Second textfield";
    floatLabelTextField2.font = [UIFont fontWithName:@"HelveticaNeue" size:20];
    [self.view addSubview:floatLabelTextField2];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
